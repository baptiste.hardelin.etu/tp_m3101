#include "fonction.h"

#define TAILLE_BUFFER 128


void execute_ligne_commande()
{
    int flag = 0;
    int nb = 0;
    char ***res = ligne_commande(&flag, &nb);

    for (int commande = 0; commande < nb; commande++)
    {
        if (strcmp(res[commande][0], "exit") == 0)
        {
            libere(res);
            exit(0);
        }
        if (fork() == 0)
        {
            execvp(res[commande][0], res[commande]);
            perror("error:");
            libere(res);
            exit(1);
        }
        if (flag == 0)
        {
            wait(NULL);
        }
        while (waitpid(-1, NULL, WNOHANG)>0){
            
        }
    }
}

void affiche_prompt()
{
    char host[256];
    char cwd[256];
  //  char *utilisateur = getenv("USER");
    gethostname(host, 256);
    getcwd(cwd, 256);
    printf("> ");
   // printf("\n%s@%s:%s$", utilisateur, host, cwd);
    fflush(stdout);
}