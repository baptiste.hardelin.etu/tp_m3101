#ifndef FONCTION_H
#define FONCTION_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "ligne_commande.h"


void execute_ligne_commande();
void affiche_prompt();
int lance_commande(int in, int out, char * com, char ** argv);

#endif