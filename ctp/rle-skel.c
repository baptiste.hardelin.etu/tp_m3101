/* A REMPLACER PAR: NOM PRENOM GROUPE MACHINE */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

#define UNUSED(x) ((void)x)

extern int decompresse(int in, int out, int *taille_source);
extern int traiter_fichier(char *fichier, int decomp);

/* Pour implémenter une fonction, décommentez là, elle remplacera la correction */
char *nouveau_nom(char *nom, int decomp)
{
  if (decomp == 1)
  {
    return strncat(nom, ".iutorig", strlen(nom));
  }
  else if (decomp == 0)
  {
    return strncat(nom, ".iutcomp", strlen(nom));
  }
  return NULL;
}

int ecrire_compression(int out, int nombre, char valeur)
{
  char *buff1[2000];
  char *buff2[2];

  if (nombre > 255)
  {
    write(out, buff2, valeur);
  }
  else
  {
    write(out, buff1, valeur);
  }
  return -1;
}

int compresse(int in, int out, int *taille_source)
{
  char buff[1024];
  int *occu = 0;
  int n;
  char lastC = ' ';

  while ((n = read(out, buff, *taille_source)) != -1)
  {
    for (int i = 0; i < n; i++)
    {
      if (buff[i] == lastC)
      {
        (*occu)++;
      }
      else
      {
        ecrire_compression(in, i, lastC);
      }

      lastC = buff[i];
    }
  }

  return out;
}

int traiter_fichier(char *fichier, int decomp)
{
  int destination;
  char *fichierDestination = fichier;
  int tauxCompression = 0;
  int taille_source = 1024;
  int n;

  while ((n = read(destination, fichier, 1024)) != 0)
  {
    nouveau_nom(fichier, decomp);
    destination = open(fichierDestination, O_WRONLY | O_CREAT | O_TRUNC);
    if (decomp == 0)
    {
      compresse(decomp, destination, &taille_source);
      tauxCompression = 100 * (compresse(decomp, destination, &taille_source) - strlen(fichierDestination) / strlen(fichier));
      printf("Compression %s => %s (taux de compression %d %%)\n", fichier, fichierDestination, tauxCompression);
      return 0;
    }
    else if (decomp == 1)
    {
      decompresse(decomp, destination, &taille_source);
      tauxCompression = 100 * (compresse(decomp, destination, &taille_source) - strlen(fichierDestination) / strlen(fichier));
      printf("Compression %s => %s (taux de compression %d %%)\n", fichier, fichierDestination, tauxCompression);
      return 0;
    }
  }
  return -1;
}

int main(int argc, char **argv)
{
  int decomp = 0;
  char **fichiers = argv + 1;
  if (fichiers[0] != NULL && strcmp(fichiers[0], "-d") == 0)
  {
    decomp = 1;
    fichiers++;
    argc--;
  }
  if (argc < 2)
  {
    fprintf(stderr, "utilisation: %s <fichier1> [fichier2 ... fichierN]\n", argv[0]);
    return 1;
  }

  for (int i = 0; i < argc - 1; i++)
  {
    if (traiter_fichier(fichiers[i], decomp) == -1)
    {
      fprintf(stderr, "%s: erreur\n", fichiers[i]);
    }
  }
  return 0;
}
