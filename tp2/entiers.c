#include "entiers.h"

void afficher(int list[], int taille) {
    for(int i = 0; i < taille; i++) {
        printf("%d", list[i]);
    }
}

int somme(int list[], int taille) {
    int calcul = 0;
    for(int i = 0; i < taille; i++) {
        calcul += list[i]; 
    }

    return calcul;
}

void copie_dans(int dest[], int src[], int taille) {
    for(int i = 0; i < taille; i++) {
        dest[i] = src[i];
    }
}

void ajoute_apres(int dest[], int taille_dest, int src[], int taille_src) {
    for(int j = 0; j < taille_src; j++) {
        dest[j + taille_dest] = src[j];
    }
}