#ifndef ENTIERS_H
#define ENTIERS_H
#include <stdio.h>

void afficher(int list[], int taille);
int somme(int list[], int taille);
void copie_dans(int dest[], int src[], int taile);
void ajoute_apres(int dest[], int taille_dest, int src[], int taille_src);
#endif