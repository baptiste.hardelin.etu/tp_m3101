#include "rat.h"

struct rat rat_produit(struct rat n1, struct rat n2) {
    struct rat res = {
        .den = n1.den * n2.den,
        .num = n1.num * n2.num
    };

    return res;
}

struct rat rat_somme(struct rat n1, struct rat n2) {
    struct rat res = {
        .den = n1.den * n2.den,
        .num = n1.num * n2.den + n2.num * n1.den,
    };

    return res;

}

struct rat rat_plus_petit(struct rat list[]) {
    struct rat plus_petit = {
        .num = 1, 
        .den = 0
    };

    int i = 0;
    while(list[i].den != 0) {
        if(i == 0) {
            plus_petit.num = list[0].num;
            plus_petit.den = list[0].den;
        }
        if((((float) list[i].num / list[i].den) < ((float) plus_petit.num / plus_petit.den))) {
            plus_petit.num = list[i].num;
            plus_petit.den = list[i].den;
        }

        i++;
    }

    return plus_petit;
}