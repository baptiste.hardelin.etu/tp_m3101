#include <stddef.h>
#include <stdio.h>
#include "chaines.h"

//Les différents strlen à stocker dans une variable

int mon_strlen_tab(char s[]) {
    int cpt = 0;
    while(s[cpt] != '\0') cpt++;
    return cpt;
}

int mon_strlen(const char *s) {
    int cpt = 0;
    while(*(s+cpt) != '\0') cpt++;
    return cpt;
}

int mon_strcmp(const char * s1, const char * s2) {
    if(s1 < s2) {
        return -1;
    }else if(s1 == s2) {
        return 0;
    }else {
        return 1;
    }
}

int mon_strncmp(const char * s1, const char * s2, int n) {
    int cpt = 0;
    int diff = 0;
    int nMax = n;
    int num = 0;

    if(mon_strlen(s1) >= mon_strlen(s2)) {
        num = 0;
    } else {
        num = 1;
    }

    if(mon_strlen(s1) < n) {
        nMax = mon_strlen(s1);
    }

    if(mon_strlen(s2) < n) {
        nMax = mon_strlen(s2);
    }

    if(n >= mon_strlen(s1) && n >= mon_strlen(s2)) {
        return mon_strcmp(s1, s2);
    }

    char car = ' ';
    while(diff == 0 && car != '\0' && cpt < nMax) {
        if(num == 0) car = *(s1 + cpt);
        else car = *(s2 + cpt);
        
        if(*(s1 + cpt) > *(s2 + cpt)) diff = 1;
        else if (*(s1 + cpt) < *(s2 + cpt)) diff = -1;

        cpt++;
    }
    return diff;
}

char *mon_strcat(char *s1, const char *s2) {
    int i = 0, j = 0;
    while (s1[i]) ++i;
    while (s2[j]) s1[i++] = s2[j++];
    s1[i] = '\0';
    return s1;

}

char *mon_strchr(const char *s, int c) {
    for(int i = 0; s[i] != '\0'; i++) {
        if(s[i] == c) {
            return (char *) s + i; // = s[i++]
        }
    }

    return NULL;
}



char *mon_strstr(const char *haystack, const char *needle) {
    int len1 = mon_strlen(haystack);
    int len2 = mon_strlen(needle);

    while (len1 >= len2) {
        len1--;
        if (!mon_strncmp(haystack, needle, len2)) {
            return (char *)haystack;
        }
        haystack++;
    }

    return NULL;
}