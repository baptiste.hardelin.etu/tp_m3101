#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "fonctions.h"
#include "tests.h"

char *miroir(const char *s)
{
    int l_s = strlen(s);
    char *res = (char *)malloc((l_s + 1) * sizeof(char));
    if (s != NULL)
    {
        int k = 0;
        for (int i = l_s - 1; i >= 0; i--)
        {
            *(res + k) = *(s + i);
            k++;
        }
    }

    *(res + l_s) = '\0';
    return res;
}

char *saisie()
{
    int cap = 1024;
    char *res = malloc(sizeof(char) * 1024);
    char c = getchar();
    int taille = 0;

    while ((isspace(c) == 0) && c != EOF)
    {
        if (taille + 1 >= cap)
        {
            cap += 1024;
            res = realloc(res, cap);
        }

        res[taille] = c;
        ++taille;
        c = getchar();
    }

    res = realloc(res, taille + 1);
    res[taille] = '\0';
    return res;
}