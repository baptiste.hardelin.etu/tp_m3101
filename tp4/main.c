#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "chaines.h"
#include "fonctions.h"

int main(int argc, char *argv[]){
    const char* prim = argv[1];
    char * chaine = malloc(sizeof(char)*64);
    for(int i =1;i<argc;++i){
        mon_strcat(chaine,(const char*)(argv[i]));    
    }
    if(argc==3 && strcmp(prim,"-m")==0){
        const char * c = argv[2];
        char * m = miroir(c);
        printf("\n%s",m);
    }else if(argc==2 && strcmp(prim,"-s")==0){
        printf("%s",saisie());
    }else if(mon_strstr(chaine,"-ms")!=NULL || mon_strstr(chaine,"-sm")!=NULL || (mon_strstr(chaine,"-m")!=NULL && mon_strstr(chaine,"-s")!=NULL) ){
        const char * s = saisie();
        printf("%s\n",miroir(s));
    }else{
        printf("mauvaise utilisation\n");
    }
    return 0;
}