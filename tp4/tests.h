#ifndef __TESTS_H__
#define __TESTS_H__

#ifdef TEST
#include <stdlib.h>
void *test_malloc(size_t size);
void test_free(void *ptr);
void *test_realloc(void *ptr, size_t size);
#define malloc test_malloc
#define free   test_free
#define realloc test_realloc
#endif

#endif
