#include <stdio.h>
#include  "main.h"

int main(int argc, char *argv[]){

    int car, lig, mot;

    int fd;


    if(argc <= 2) {
        fd = STDIN_FILENO;
    }

    fd = open(argv[argc-1], O_RDONLY);
    traiter(fd, &car, &mot, &lig);

    for(int i = 1; i < argc; i++) {
        if(strstr(argv[i], "l")) {
            printf("Nombre de ligne : %d\n", lig);
        } else if(strstr(argv[i], "w")) {
            printf("Nombre de mot : %d\n", mot);
        } else if(strstr(argv[i], "c")) {
            printf("Nombre de caractère : %d\n", car);
        } else if(argc == 2 ) {
            printf("Nombre de ligne : %d\n", lig);
            printf("Nombre de mot : %d\n", mot);
            printf("Nombre de caractère : %d\n", car);
        }
    }

    return 0;
}
