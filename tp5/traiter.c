#include "traiter.h"

#define BUFFER_SIZE 80
int traiter(int f, int *car, int *mot, int *lig) {
    char c[BUFFER_SIZE];

    *car = 0;
    *mot = 0;
    *lig = 0;
    char lastC = ' ';

    int n;
    int i = 0;
    while ((n = read(f, c, BUFFER_SIZE)) != 0) {
        for(i; i < n; i++) {
            
            if(c[i] == '\n') {
               (*lig)++;
            }

            if(!isspace(c[i]) && isspace(lastC)){
                (*mot)++;
            }

            lastC = c[i];
            (*car)++;
        }
        /* printf("lignes     : %d\n", *lig);
        printf("mots       : %d\n", *mot);
        printf("caractères : %d\n", *car);*/
    }
    return 0;
}