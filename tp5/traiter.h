#ifndef TRAITER_H
#define TRAITER_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

int traiter (int f, int *car, int *mot, int *lig);

#endif