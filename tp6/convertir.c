#include "convertir.h"
#include <unistd.h>

const char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
							   'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
							   'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
							   'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
							   'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
							   'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
							   'w', 'x', 'y', 'z', '0', '1', '2', '3',
							   '4', '5', '6', '7', '8', '9', '+', '/'};

char convertir(char valeur)
{
	if (valeur < 0 || valeur > 63)
	{
		return -1;
	}
	
	return encoding_table[(int)valeur];
}

void encoder_bloc(const char* source, int taille_source, char* destination)
{
	if (taille_source == 3)
	{
		destination[0] = convertir(source[0] >> 2 & 0b111111);
		destination[1] = convertir(((source[0] & 0b11) << 4) | (source[1] >> 4 & 0b1111));
		destination[2] = convertir(((source[1] & 0b1111) << 2) | (source[2] >> 6 & 0b11));
		destination[3] = convertir(source[2] & 0b111111);
	}
	else if (taille_source == 2)
	{
		destination[0] = convertir(source[0] >> 2 & 0b111111);
		destination[1] = convertir(((source[0] & 0b11) << 4) | (source[1] >> 4 & 0b1111));
		destination[2] = convertir((source[1] & 0b1111) << 2);
		destination[3] = '=';
	}
	else if (taille_source == 1)
	{
		destination[0] = convertir(source[0] >> 2 & 0b111111);
		destination[1] = convertir((source[0] & 0b11) << 4);
		destination[2] = '=';
		destination[3] = '=';
	}
}

int encoder_fichier(int source, int destination)
{
	char buffer_reader[BUFFER_SIZE_READER];
	
	int bytes = read(source, buffer_reader, BUFFER_SIZE_READER);
	if (bytes == -1)
	{
		return -1;
	}

	int bytes_destinataire;
	while (bytes > 0)
	{
		char buffer_writer[BUFFER_SIZE_WRITER];
		encoder_bloc(buffer_reader, bytes, buffer_writer);
		bytes_destinataire = write(destination, buffer_writer, 4);
		
		if (bytes_destinataire == -1)
		{
			return -1;
		}

		int end = 0;
		for (int i = 0; source == 0 && i < BUFFER_SIZE_READER; i++)
		{
			if (buffer_reader[i] == '\n')
			{
				end = 1;
			}
		}

		if (end == 1)
		{
			bytes = 0;
		}
		else
		{
			bytes = read(source, buffer_reader, BUFFER_SIZE_READER);
		}
	}

	return 0;
}