#ifndef FONCTIONS_H
#define FONCTIONS_H

#define BUFFER_SIZE_READER 3
#define BUFFER_SIZE_WRITER 4

char convertir(char valeur);
void encoder_bloc(const char *source, int taille_source, char *destination);
int encoder_fichier(int source, int destination);

#endif