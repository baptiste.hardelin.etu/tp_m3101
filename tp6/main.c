#include "main.h"
#include "convertir.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int source;
int destination;

int manageArguments(int argc, char* argv[])
{
    if (argc > 1)
    {
        if (argc == 2)
        {
            source = open(argv[1], O_RDONLY);
            destination = STDOUT_FILENO;
            return 0;
        }
        else if (argc == 3)
        {
            source = open(argv[1], O_RDONLY);
            destination = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC);
            return 0;
        }
    }
    else
    {
        source = STDIN_FILENO;
        destination = STDOUT_FILENO;
        return 0;
    }
    return 1;
}
int manageErrors(int argc, char* argv[])
{
    if (manageArguments(argc, argv))
    {
        fprintf(stderr, "Error: Too much arguments\n");
        return 1;
    }
    if (source == -1)
    {
        perror(argv[1]);
        return 1;
    }
    if (destination == -1)
    {
        perror(argv[2]);
        return 1;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc == 2 && (strcmp("-h", argv[1]) || strcmp("--help", argv[1])))
    {
        printf("b64encode <source> <destination>\n\t<source> = stdin\n\t<destination> = stdout\n");
    } 
    else if (manageErrors(argc, argv))
    {
        return 1;
    }
    
    encoder_fichier(source, destination);
    printf("\n\n");

    return 0;
}